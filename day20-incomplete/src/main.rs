use std::rc::{Rc, Weak};
use std::cell::RefCell;

// A looping doubly-linked list
struct LLL<T> {
    pub origin: Link<T>,
    pub all_nodes: Vec<Rc<RefCell<Node<T>>>>,
}

// using option so the links can be null while creating them, they shouldn't be null while in use.
type Link<T> = Option<Rc<RefCell<Node<T>>>>;
// The reference to the previous node has to be weak, so that we can have a loop.
type WeakLink<T> = Option<Weak<RefCell<Node<T>>>>;

struct Node<T> {
    pub data: T,
    pub next: Link<T>,
    pub prev: WeakLink<T>,
}

impl<T> LLL<T> {
    fn new_from(mut data: Vec<T>) -> Self {
        let last = data.pop().map(Node::new);
        let mut nodes: Vec<_> = data.drain(..)
            .scan((|| {last.unwrap()})(), |prev, elem| {
                let node = Node::new(elem);
                prev.borrow_mut().next = Some(node.clone();
                Some(node)
            })
            .collect();
        // set up links
        for w in nodes.as_mut_slice().windows(3) {
            match w {
                [prev, cur, next] => {
                    let x: () = cur;
                    let cur = cur.get_mut();
                    cur.prev = Some(Rc::downgrade(prev));
                    cur.next = Some(next.clone());
                }
                _ => unreachable!(),
            }
        }
        Self {
            origin: nodes.get(0).map(Rc::clone),
            all_nodes: nodes,
        }
    }
}

impl<T> std::fmt::Debug for LLL<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        //TODO: print each node and the no. of pointers to it
        todo!()
    }
}

impl<T> Node<T> {
    fn new(data: T) -> Rc<RefCell<Self>> {
        Rc::new(RefCell::new(Self {
            data: data,
            next: None,
            prev: None,
        }))
    }
}

fn main() {
    println!("Hello, world!");
}
