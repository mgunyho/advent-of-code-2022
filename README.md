# Advent of code 2022

My initial idea for this year's [Advent of Code](https://adventofcode.com/2022) was to solve problems with a different programming language every day. So I did the first day with [BQN](https://mlochbaum.github.io/BQN/). But then I fell in love with BQN, and ended up solving most problems using it. Some problems aren't so well suited for array-oriented programming, in which case I'll use some other language that is fun, like Rust.
