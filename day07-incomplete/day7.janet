(def commands
    (string/split "\n"
        (file/read (file/open "example.txt") :all)))

# remove the last element (empty line)
(array/pop commands)

(pp commands)

(defn parse-commands
    "parse a list of commands into a file tree structure"
    [commands]
    (defn inner [tree remaining-commands]
        (let [[head & tail] remaining-commands]
            (if (string/startswith "$ cd" head) "a" "b")
            ))
    (inner {} commands))

(pp (parse-commands commands))

# if a struct has the key "children", it's a dir, otherwise it's a file

