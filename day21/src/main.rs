use std::collections::HashMap;


#[derive(Clone, Debug)]
enum Expr {
    Value(u64),
    Symbol(String),
    // lhs, operation, rhs
    Op(Box<Expr>, char, Box<Expr>)
}

impl Expr {

    // Evaluate an expression. Replace all symbol expressions in the symbol table 'tree'
    // recursively to yield a single Value expression.
    fn evaluate_sym(tree: &HashMap<String, Self>, sym: &Self, part2: bool) -> Self {
        match sym {
            Self::Value(_) => sym.clone(),
            Self::Symbol(s) => {
                if part2 && s == "humn" {
                    // if we're doing part 2, don't evaluate 'humn' but keep it as a Symbol
                    sym.clone()
                } else {
                    Self::evaluate_sym(&tree, &tree[s], part2)
                }
            }
            Self::Op(l, op, r) => {
                let l = Self::evaluate_sym(tree, &l, part2);
                let r = Self::evaluate_sym(tree, &r, part2);

                match op {
                    '+' => l + r,
                    '-' => l - r,
                    '*' => l * r,
                    '/' => l / r,
                    _ => unreachable!(),
                }
            }
        }
    }

    // part 2: find the value of the Symbol in self such that evaluating self is equal to target.
    // Equivalently, solve x in f(x) = y, where f(x) is the expression self, and y is the target.
    // Assuming that there's only one unevaluated symbol in self.
    fn solve(&self, target: &Self) -> Self {
        match self {
            // we only have a symbol on the LHS, we're done
            Self::Symbol(_) => target.clone(),
            Self::Op(l, op, r) => {
                // dereference
                let l = &**l;
                let r = &**r;

                let op_inv = match op {
                    '+' => '-',
                    '-' => '+',
                    '*' => '/',
                    '/' => '*',
                    _ => unreachable!(),
                };

                if let Self::Value(_) = l {
                    // l is a value, so the symbol must be within r.
                    // Here we have to be careful with the operations and their order: for example
                    // if the situation is
                    // 5 / x = 10, we should in fact invert the RHS To get
                    // x/5 = 1 / 10 -> x = 5 / 10.
                    // Similarly for 5 - x = 10 => x = 5 - 10.
                    // OTOH, 5 + x = 10 -> x = 10 - 5 and 5 * x = 10 -> x = 10 / 5
                    let new_target = match op {
                        '-' | '/' => Expr::Op(l.clone().into(), *op, target.clone().into()),
                        _ => Expr::Op(target.clone().into(), op_inv, l.clone().into()),
                    };
                    r.solve(&new_target)
                } else if let Self::Value(_) = r {
                    // r is a value, symbol must be within l. Operate from the right with the
                    // inverse operation.
                    l.solve(&Expr::Op(target.clone().into(), op_inv, r.clone().into()))
                } else {
                    unreachable!()
                }
            }
            Self::Value(_) => panic!("can't invert value {:?}", &self),
        }
    }
}


// shorthand for implementing std::ops::Add/Sub/Mul/Div for Expr
macro_rules! impl_op {
    ( $sym:tt, $symchar:tt, $trait:ident, $trait_fn:ident ) => {
        impl std::ops::$trait for Expr {
            type Output = Self;
            fn $trait_fn(self, other: Self) -> Self {
                match (&self, &other) {
                    (Self::Value(x), Self::Value(y)) => {
                        Self::Value(x $sym y)
                    },
                    _ => Expr::Op(Box::new(self), $symchar, Box::new(other))
                }
            }
        }

    }
}

// In theory we wouldn't need the ' ' versions and lowercase versions (the macro should do it), but
// it would require some extra complication stuff so won't bother.
impl_op!(+, '+', Add, add);
impl_op!(-, '-', Sub, sub);
impl_op!(*, '*', Mul, mul);
impl_op!(/, '/', Div, div);

impl From<&str> for Expr {
    fn from(s: &str) -> Self {
        Self::Symbol(s.to_string())
    }
}

// parse the top-level expression from a list of strings
fn parse_tree(lines: &[&str]) -> HashMap<String, Expr> {
    lines.iter()
        .map(|line| {
            let parts: Vec<_> = line.split(": ").collect();
            let key = parts[0];
            let exp = if let Ok(n) = parts[1].parse::<u64>() {
                Expr::Value(n)
            } else {
                let parts: Vec<_> = parts[1].split(" ").collect();
                Expr::Op(
                    Box::new(parts[0].into()),
                    parts[1].chars().next().unwrap(),
                    Box::new(parts[2].into()),
                )
            };
            (key.to_string(), exp)
        }).collect()
}

fn main() {
    //let fname = "example.txt";
    let fname = "input.txt";
    let input = std::fs::read_to_string(fname).unwrap();
    let mut tree = parse_tree(&input.as_str().lines().collect::<Vec<&str>>());

    println!("part1: {:?}", Expr::evaluate_sym(&mut tree, &"root".into(), false));

    // part 2
    // The two values that need to match
    let (l, r) = match &tree["root"] {
        Expr::Op(l, _, r) => (
            Expr::evaluate_sym(&tree, &l, true),
            Expr::evaluate_sym(&tree, &r, true)
        ),
        _ => unreachable!(),
    };

    // expression that is the solution to part 2
    let sol2 = l.solve(&r);

    // evaluate the final result into a final Value using evaluate_sym with an empty symbol table
    println!("part2: {:?}", Expr::evaluate_sym(&HashMap::new(), &sol2, false));

}
