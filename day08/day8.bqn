⟨SL ⇐ SplitLines⟩ ← •Import "../utils.bqn"

input ← •FBytes "example.txt"
#input ← •FBytes "input.txt"

# all values are single digits, no need for string -> number conversion
data ← >(SL input) - '0'

# given a matrix of trees, mark the trees that are visible when looking from the top.
# to find trees that are visible from other directions, reverse or transpose the input
Visible_from_top ← {
  # a row of ones that has the same size as the first row of x
  ones ← =˜⊏𝕩
  # take '<' between the running maximum and the original data shifted to the
  # left by 1, this gives all internal trees that are visible. Shift the result
  # to the right by 1 and add the row of ones (all trees at the top are
  # visible) to get the final result.
  ones » (⌈`𝕩) < «𝕩
}

# shorthand
V ← Visible_from_top

# V - find trees visible from the top
# V⌾⌽ - reverse, V, reverse - visible from the bottom
# V⌾⍉ - transpose, V, transpose - visible from the left
# V⌾(⌽∘⍉) - transpose & reverse, V, reverse & transpose - visible from the right
#
# Put all of the above functions in a list and apply each of them to the data.
# Reduce the resulting list of 2D arrays with 'or', flatten the result and take
# the sum to find the no. of visible trees, i.e. answer to part 1.
•Show +´⥊∨´V‿(V⌾⌽)‿(V⌾⍉)‿(V⌾(⌽∘⍉)) {𝕎𝕩}¨  <data

# Find out how many trees are visible when looking downwards from a treehouse
# placed on each column in the top row of 𝕩.
Visible_from_treehouse_down ← {
  #•Show ⊏𝕩
  #•Show 1↓𝕩

  #(⊏𝕩) ≤ ⍉𝕩
  #(⊏𝕩)⊸>⌾⍉𝕩
  #•Show ≤`⍉(⍉1↓𝕩)<⊏𝕩

  #•Show (0×⊏𝕩)≤`⍉(⍉1↓𝕩)<⊏𝕩
  #•Show <´⍉(⍉1↓𝕩)<⊏𝕩
  #⍉∊˘(⍉𝕩)<⊏𝕩


  #•Show 𝕩
  #•Show ⍉(⍉1↓𝕩)<⊏𝕩
  #•Show 1+˝(=˜⊏𝕩)⌊`⍉(⍉1↓𝕩)<⊏𝕩
  #TODO: need to append a row of zeros at the end at some point? to fix boundaries
  #•Show (=˜⊏𝕩)⌊`⍉(⍉1↓𝕩)<⊏𝕩
  #1+˝(=˜⊏𝕩)⌊`⍉(⍉1↓𝕩)<⊏𝕩  # <- best as of 10:41 (I think)
  #1+˝(((=˜⊏𝕩)⌊`⍉(⍉1↓𝕩)<⊏𝕩)∾(≠˜⊏𝕩)) # attempt at adding zeros, not good
  #•Show ((=˜⊏𝕩)⌊`⍉(⍉(10×=˜⊏𝕩)«𝕩)<⊏𝕩)
  #0+˝((=˜⊏𝕩)⌊`⍉(⍉(10×=˜⊏𝕩)«𝕩)<⊏𝕩) # add 10's at the bottom

  # new attempt
  ones ← =˜⊏𝕩
  zeros ← ≠˜⊏𝕩
  #•Show ⊏𝕩
  #•Show 𝕩
  #•Show +´˘⍉ •Show ones » •Show ones ⌊` « ⍉(⍉𝕩)<⊏𝕩
  #•Show +´˘⍉ •Show ones » •Show ⌊` ⍉(⍉1↓𝕩)<⊏𝕩
  #•Show +´˘⍉ •Show ones » ones⌊` •Show « ⍉(•Show (⍉𝕩∾ones×10))<⊏𝕩
  +´˘⍉ ones » ⌊` ⍉(⍉1↓𝕩)<⊏𝕩
}

# Compute the viewing distances downwards for treehouses placed in all trees in 𝕩
Viewing_distances_down ← {
  > Visible_from_treehouse_down ¨ ¯1↓↓ 𝕩
}


# shorthand
VD ← Viewing_distances_down

#•Show Visible_from_treehouse_down data


#•Show data
#•Show VD ⌾ (⊢) data
#•Show VD ⌾ (⍉) data
#•Show VD ⌾ (⌽) data
#•Show VD ⌾ (⌽∘⍉) data

•Show ⌈´ ⥊ ×´{𝕏data} ¨ (VD ⌾ (⊢))‿(VD ⌾ (⍉))‿(VD ⌾ (⌽))‿(VD ⌾ (⌽∘⍉))

#•Show VD data
#•Show VD ¨ <data
#•Show VD‿(VD⌾⌽)‿(VD⌾⍉)‿(VD⌾(⌽∘⍉)) {𝕎𝕩}¨ <data
#•Show ≢ VD‿(VD⌾⌽)‿(VD⌾⍉)‿(VD⌾(⌽∘⍉)) {𝕎𝕩}¨ <data
#•Show ×´ VD‿(VD⌾⌽)‿(VD⌾⍉)‿(VD⌾(⌽∘⍉)) {𝕎𝕩}¨ <data
•Show ≢ data
