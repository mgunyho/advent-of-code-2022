#[derive(Debug, PartialEq)]
enum Packet {
    Data(u8),
    List(Vec<Packet>),
}

impl<'a> Packet {

    /// Parse a line like '[1, [2], 3]' into a Packet object.
    /// All sub-parse methods take byte slices to make life (i.e. indexing) easier.
    pub fn parse(line: &str) -> Self {
        // assume that the outermost item is always a list, i.e. the line always starts with a
        // '[', so skip the first char/byte
        Packet::parse_list(&line[1..].as_bytes()).0
    }

    /// Parse a data item (i.e. a number)
    fn parse_data(remaining: &'a [u8]) -> (Packet, &'a [u8]) {
        let mut i = 0; // index into the string
        let mut data_chars: Vec<u8> = Vec::with_capacity(2); // packet data contains at most two digits
        loop {
            let c = remaining[i];
            if c.is_ascii_digit() {
                data_chars.push(c);
            } else {
                let data_str = String::from_utf8(data_chars).unwrap();
                return (Packet::Data(data_str.parse().unwrap()), &remaining[i..])
            }
            i += 1;
        }
    }

    /// Parse a nested packet
    fn parse_list(remaining: &'a [u8]) -> (Packet, &'a [u8]) {
        let mut result: Vec<Packet> = vec![];
        let mut rest = remaining;

        // i = current index in the string. Can't use iterators because we have to step backwards
        // when parsing data, so use index manually.
        let mut i = 0;
        loop {
            if rest.len() == 0 {
                break;
            }
            let inner = match rest[i] {
                b'[' => Some(Packet::parse_list(&rest[i+1..])),
                b',' => None,
                b']' => { i += 1; break; },
                _ => Some(Packet::parse_data(&rest[i..])),
            };

            if let Some((packet, r)) = inner {
                result.push(packet);
                rest = r;
                i = 0;
            } else {
                i += 1;
            }
        }
        (Packet::List(result), &rest[i..])
    }
}

impl PartialOrd for Packet {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        match (self, other) {
            (Packet::Data(n), Packet::Data(m)) => n.partial_cmp(m),
            (Packet::List(p), Packet::List(q)) => {
                for (a, b) in p.iter().zip(q.iter()) {
                    let cmp = a.partial_cmp(b);
                    if cmp.is_some() && cmp != Some(std::cmp::Ordering::Equal) {
                        return cmp;
                    }
                }
                // all comparisons were equal, return whether the left list was shorter
                return p.len().partial_cmp(&q.len())
            },
            (Packet::Data(a), _) => {
                Packet::List(vec![Packet::Data(*a)]).partial_cmp(other)
            },
            (_, Packet::Data(b)) => {
                self.partial_cmp(&Packet::List(vec![Packet::Data(*b)]))
            }
        }
    }
}

fn main() {

    //let fname = "example.txt";
    let fname = "input.txt";

    let data = std::fs::read_to_string(fname).unwrap().trim().to_string();

    let packet_pairs: Vec<Vec<_>> = data
        .split("\n\n")
        .map(|line_pair| { line_pair.split('\n').map(Packet::parse).collect() })
        .collect();

    let part1: usize = packet_pairs
        .iter()
        .enumerate()
        .filter_map(|(i, pair)| { if pair[0] < pair[1] { Some(i+1) } else { None } })
        .sum();

    println!("part 1: {:?}", part1);

    let dividers = vec![Packet::parse("[[2]]"), Packet::parse("[[6]]")];
    let mut packets_with_extra: Vec<_> = packet_pairs
        .iter()
        .flatten()
        .chain(dividers.iter()) // append divider packets
        .collect();

    // Can't use regular sort, because it requires total ordering, which I'm too lazy to implement,
    // so use sort_by with partial_cmp instead.
    packets_with_extra.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());

    let part2: usize = packets_with_extra
        .iter()
        .enumerate()
        .filter_map(|(i, p)| if *p == &dividers[0] || *p == &dividers[1] {
            Some(i+1)
        } else {
            None
        })
        .reduce(|a, b| a*b)
        .unwrap();
    println!("part 2: {:?}", part2);
}
