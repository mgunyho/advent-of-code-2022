## day 13: Rust

Yeah, I'm not gonna do pattern matching with BQN. So instead, I'll do today using a language with excellent pattern matching: Rust. To make things a little bit more challenging, I implemented the packet parsing myself so that it allocates as little memory as possible (as far as I can tell).


Here's a small BQN snippet that takes a string like `[[1],2]` and converts it into a nested BQN list like `⟨⟨1⟩ 2⟩`:
```BQN
Convert_list ← {
  # map '[' to 1, ']' to 2 and everything else to 0 in 𝕩, and use choose to convert them to ⟨ and ⟩
  •BQN {(1×'['=𝕩) + 2×']'=𝕩}◶⟨⊢, '⟨', '⟩'⟩¨𝕩
}
```

That's all I did using BQN for today.
