# Day 1: [BQN](https://mlochbaum.github.io/BQN/)

I have to start with the crazy stuff, while it's still possible. I also considered brainfuck, but I thought today would be easy with an array language.

I used the [CBQN](https://github.com/dzaima/CBQN) implementation of BQN.
